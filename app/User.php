<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Hash;

class User extends Model
{
    use SoftDeletes;

    const TYPE_PATIENT = 1;
    const TYPE_DOCTOR = 2;

    public static function createPatient($patient){
        $user = new User([
            'email' => $patient['email'],
            'fname' => $patient['fname'],
            'lname' => $patient['lname'],
            'contact' => $patient['contact'],
            'address' => $patient['address'],
            'dob' => $patient['dob'],
            'gender' => $patient['gender'],
            'type' => User::TYPE_PATIENT
        ]);

        $user->save();

        return $user;
    }

    public static function createDoctor($doctor){
        $user = new User([
            'email' => $doctor['email'],
            'fname' => $doctor['fname'],
            'lname' => $doctor['lname'],
            'contact' => $doctor['contact'],
            'address' => $doctor['address'],
            'dob' => $doctor['dob'],
            'gender' => $doctor['gender'],
            'type' => User::TYPE_DOCTOR
        ]);

        $user->save();

        return $user;
    }

    public static function getPatients($keyword){
        return User::where('type', User::TYPE_PATIENT)
            ->where('fname', 'LIKE', '%' . $keyword . '%')
            ->get();
    }

    public static function getDoctors($keyword){
        return User::where('type', User::TYPE_DOCTOR)
            ->where('fname', 'LIKE', '%' . $keyword . '%')
            ->get();
    }

    public function authenticatePatient($email, $password){
        $user = User::where('email','LIKE', $email)
            ->where('type', User::TYPE_PATIENT)
            ->first();

        if(!empty($user)){
            $user = $user->makeVisible('password')->toArray();

            if(Hash::check($password, $user['password'])){
                $user = User::where('email','LIKE', $email)
                    ->first()->makeHidden('password');

                return ['success' => true, 'user' => $user, 'status' => 200];
            }
            else{
                return [
                    'success' => false,
                    'message' => 'Invalid username or password.'
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => 'Invalid username or password'
            ];
        }
    }

    public function authenticateDoctor($email, $password){
        $user = User::where('email','LIKE', $email)
            ->where('type', User::TYPE_DOCTOR)
            ->first();

        if(!empty($user)){
            $user = $user->makeVisible('pssword')->toArray();

            if(Hash::check($password, $user['password'])){
                $user = User::where('email','LIKE', $email)
                    ->first()->makeHidden('password');

                return ['success' => true, 'user' => $user, 'status' => 200];
            }
            else{
                return [
                    'success' => false,
                    'message' => 'Invalid username or password.'
                ];
            }
        }
        else{
            return [
                'success' => false,
                'message' => 'Invalid username or password'
            ];
        }
    }
}
