<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorQualificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctor_qualifications', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->bigInteger('doctor_id')->unsigned();
            $table->bigInteger('qualification_id')->unsigned();
            $table->string('institute');
            $table->string('year');

            $table->foreign('doctor_id')->references('id')->on('users');
            $table->foreign('qualification_id')->references('id')->on('qualifications');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctor_qualifications');
    }
}
